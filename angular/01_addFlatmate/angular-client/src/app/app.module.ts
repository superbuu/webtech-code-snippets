import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnlargeDirective } from './enlarge.directive';
import { TempInfoDirective } from './temp-info.directive';
import { AddFlatmateComponent } from './add-flatmate/add-flatmate.component';
import { AddPurchaseComponent } from './add-purchase/add-purchase.component';
import { BalancesComponent } from './balances/balances.component';
import { ShowPurchasesComponent } from './show-purchases/show-purchases.component';

@NgModule({
  declarations: [
    AppComponent,
    EnlargeDirective,
    TempInfoDirective,
    AddFlatmateComponent,
    AddPurchaseComponent,
    BalancesComponent,
    ShowPurchasesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

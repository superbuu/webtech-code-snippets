import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFlatmateComponent } from './add-flatmate.component';

describe('AddFlatmateComponent', () => {
  let component: AddFlatmateComponent;
  let fixture: ComponentFixture<AddFlatmateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFlatmateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFlatmateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

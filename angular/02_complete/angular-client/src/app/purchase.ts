export class Purchase {
    id: number;
    title: string;
    description: string;
    cost: number;
    date: string;
    boughtBy: string;
    boughtFor: string[];
}
var express = require('express');

var app = express();

var bodyParser = require('body-parser');

//app.use(bodyParser.urlencoded({ extended: true })); //form-urlencoded
app.use(bodyParser.json());
/*
app.use('/', function(req, res){
    res.sendFile(__dirname + '/angular-client/dist/index.html');
});//*/

app.use('/', express.static(__dirname + '/angular-client/dist/angular-client/'));

app.use('/api/v1/purchases', require('./routes/purchasesRoute.js'));
app.use('/api/v1/flatmates', require('./routes/flatmatesRoute.js'));


app.use(function(req, res, next) {// catch 404
 	var err = new Error('Wrong Path');
 	err.status = 404;
    next(err);
});

app.listen(8080, console.log('App listening on port 8080!'));
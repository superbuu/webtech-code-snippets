let express = require('express');
let router = express.Router();
let MongoClient = require('mongodb').MongoClient;

let config = require('../../config.json');


// GET list of all events
router.get('/', function(req, res, next) {
  MongoClient.connect(config.dbURL, (err, db) => {
    if(err) throw err;
    let categorieCollection = db.collection('categories');

    categorieCollection.find().toArray( (err, array) => {
      if (err) throw err;
      res.json(array);
      db.close();
    });
  });
});

module.exports = router;

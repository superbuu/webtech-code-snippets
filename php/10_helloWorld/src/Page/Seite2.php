<?php
namespace Page;

require_once(__DIR__ .'/PageInterface.php');
use \Page\PageInterface;

class Seite2 implements PageInterface {
    public function getTitle( ) {
        return 'MiniBsp-2.Seite';
    }

    public function getViewScript( ) {
        return __DIR__.'/../../view/Seite2.phtml';
    }

    public function getViewVariables() {
        return [ 'name' => 'Mini', 'index' => '2.' ];
    }
}
?>
